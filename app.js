const express = require('express');
const mainRoutes = require('./src/routes/mainRoutes');
const logger = require('./src/middlewares/logger');

const app = express();

// Middlewares

app.use(logger);

// Rotas
app.use('/', mainRoutes);

app.listen(3000);

module.exports = app;
